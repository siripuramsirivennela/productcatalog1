import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";

import getAllProducts from "./pathhandlers/getAllProducts.js";
import postProducts from "./pathhandlers/postProducts.js";
import deleteProduct from "./pathhandlers/deleteProduct.js";
import updateProduct from "./pathhandlers/updateProduct.js";

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.get('/products',getAllProducts)

app.post('/add_products',postProducts)

app.delete('/delete_product/:_id',deleteProduct)
app.put('/update_product/:_id',updateProduct)
// app.listen(8080,() => {
//     console.log("server is running");
// })

mongoose.connect('mongodb+srv://shobasiripuram791:bNjRqu9rbcrYsI3x@cluster0.yygvl14.mongodb.net/?retryWrites=true&w=majority').then(() => {
    app.listen(8080,() => {
        console.log("server is up and running...");
    })
})
.catch((err) => {
    console.log("unable to connect to mongodb...",err);
})
