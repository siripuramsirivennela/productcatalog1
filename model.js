import mongoose from "mongoose";

const productSchema = new mongoose.Schema({
    _id: {type:Number,required:true},
    Name: {type:String,requierd:true},
    desc:{type:String,required:true},
    price:{type:Number,required:true},
    stock:{type:Number,required:true},
    rating:{type:Number, default: 0}
})

const ProductModel = mongoose.model("PRODUCTMODEL",productSchema);

export {ProductModel};


