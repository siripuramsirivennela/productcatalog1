import { ProductModel } from "../model.js";

const updateProduct = (req,res) => {

    const { _id } = req.params;
       ProductModel.findById( _id ).then((data) => {
        let { Name,desc,price,stock,rating } = data;
        if (req.body.price) {
            price = req.body.price
        }
        if (req.body.stock) {
            stock = req.body.stock
        }
        if (req.body.rating) {
            rating = req.body.rating
        }
        ProductModel.findByIdAndUpdate(_id, {
            Name, desc, price, stock, rating
        }).then(() => {
            res.status(201).send({message: "Product details updated successfully..."})
        }).catch((errr) => {
            res.status(500).send({message: "Product details not updated..."})
        })
    })
    .catch((err) => {
        res.status(500).send({message: "Something went wrong..."})
    })

}

export default updateProduct;
