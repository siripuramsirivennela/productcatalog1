import {ProductModel} from "../model.js";

const getAllProducts = (req, res) => {
  ProductModel.find().then((data) => {
   res.status(200).send(data) 

  })
  .catch((err) => {
    res.status(500).send(err)

  })
};

export default getAllProducts;
