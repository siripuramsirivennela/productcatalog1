import { ProductModel } from "../model.js";

const deleteProduct = (req, res) => {
  const { _id } = req.params;

  ProductModel.findOneAndDelete({ _id: _id })
    .then((productData) => {
      res.status(200).send("Product " + productData._id + " has been deleted succesfully");
    })
    .catch((err) => {
        res.status(502).send(err)
    });
};

export default deleteProduct;
