import {ProductModel} from "../model.js";

const postProducts = (req,res) => {
    const {_id, Name, productPrice, desc, stock, rating} = req.body;
    const productDetails = {_id, Name, price:productPrice, desc, stock, rating} 
    const newProduct = new ProductModel(productDetails)
    
    newProduct.save((err, result) => {
        if (err) {
            res.status(500).send({message: "Unable to add product...",err})
        }
        else {
            res.status(200).send({message: "Product has been added successfully..."})
        }
    })
}

export default postProducts;